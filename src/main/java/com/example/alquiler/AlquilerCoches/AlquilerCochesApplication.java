package com.example.alquiler.AlquilerCoches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlquilerCochesApplication {

	public static void main(String[] args) {
				SpringApplication.run(AlquilerCochesApplication.class, args);
	}

}
