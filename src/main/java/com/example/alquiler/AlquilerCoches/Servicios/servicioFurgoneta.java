package com.example.alquiler.AlquilerCoches.Servicios;

import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import com.example.alquiler.AlquilerCoches.Modelos.Furgoneta;
import com.example.alquiler.AlquilerCoches.Repositorio.CocheRepositorio;
import com.example.alquiler.AlquilerCoches.Repositorio.FurgonetaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class servicioFurgoneta {
    @Autowired
    FurgonetaRepositorio furgonetaRepositorio;
    public boolean crearFurgoneta(Furgoneta furgoneta){
        furgoneta.setTotal(furgoneta.monto());
        Furgoneta furgonetas=furgonetaRepositorio.save(furgoneta);
        if(furgonetaRepositorio.findById(furgonetas.getId())!=null){
            return true;
        }
        return false;
    }
    public Optional<Furgoneta> GetFurgonetaById(Integer id) {
        Optional<Furgoneta> furgonetaById = furgonetaRepositorio.findById(id);
        furgonetaById.orElseThrow(() -> new IllegalArgumentException("Not found"));
        return furgonetaById;
    }

    public List<Furgoneta> getAllFurgonetas() {
        List<Furgoneta> allFurgonetas = new ArrayList<>();
        furgonetaRepositorio.findAll().forEach(furgoneta -> allFurgonetas.add(furgoneta));
        return allFurgonetas;
    }
}
