package com.example.alquiler.AlquilerCoches.Servicios;

import com.example.alquiler.AlquilerCoches.Modelos.Camion;

import com.example.alquiler.AlquilerCoches.Repositorio.CamionRepositorio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class servicioCamion {
    @Autowired
    CamionRepositorio camionRepositorio;
    public boolean crearCamion(Camion camion){
        camion.setTotal(camion.monto());
        Camion camiones=camionRepositorio.save(camion);
        if(camionRepositorio.findById(camiones.getId())!=null){
            return true;
        }
        return false;
    }
    public Optional<Camion> GetCamionById(Integer id) {
        Optional<Camion> camionById = camionRepositorio.findById(id);
        camionById.orElseThrow(() -> new IllegalArgumentException("Not found"));
        return camionById;
    }

    public List<Camion> getAllCamiones() {
        List<Camion> allcamiones = new ArrayList<>();
        camionRepositorio.findAll().forEach(camion -> allcamiones.add(camion));
        return allcamiones;
    }
}
