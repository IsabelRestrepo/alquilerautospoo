package com.example.alquiler.AlquilerCoches.Servicios;

import com.example.alquiler.AlquilerCoches.Modelos.Microbus;
import com.example.alquiler.AlquilerCoches.Repositorio.MicrobusRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class servicioMicrobus {
    @Autowired
    MicrobusRepositorio microbusRepositorio;
    public boolean crearMicrobus(Microbus microbus){
        microbus.setTotal(microbus.monto());
        Microbus microbuses=microbusRepositorio.save(microbus);
        if(microbusRepositorio.findById(microbuses.getId())!=null){
            return true;
        }
        return false;
    }
    public Optional<Microbus> GetMicrobusById(Integer id) {
        Optional<Microbus> microbusById = microbusRepositorio.findById(id);
        microbusById.orElseThrow(() -> new IllegalArgumentException("Not found"));
        return microbusById;
    }

    public List<Microbus> getAllmicrobuss() {
        List<Microbus> microbuses = new ArrayList<>();
        microbusRepositorio.findAll().forEach(microbus -> microbuses.add(microbus));
        return microbuses;
    }
}
