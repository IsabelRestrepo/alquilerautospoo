package com.example.alquiler.AlquilerCoches.Servicios;

import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import com.example.alquiler.AlquilerCoches.Repositorio.CocheRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class servicioCoche {
    @Autowired
    CocheRepositorio cocheRepositorio;

    public boolean crearCoche(Coche coche) {
        coche.setTotal(coche.monto());
        Coche coches = cocheRepositorio.save(coche);
        if (cocheRepositorio.findById(coches.getId()) != null) {
            return true;
        }
        return false;
    }

    public Optional<Coche> GetCarroById(Integer id) {
        Optional<Coche> cocheById = cocheRepositorio.findById(id);
        cocheById.orElseThrow(() -> new IllegalArgumentException("Not found"));
        return cocheById;
    }

    public List<Coche> getAllCoches() {
        List<Coche> allcoches = new ArrayList<>();
        cocheRepositorio.findAll().forEach(coche -> allcoches.add(coche));
        return allcoches;
    }

}
