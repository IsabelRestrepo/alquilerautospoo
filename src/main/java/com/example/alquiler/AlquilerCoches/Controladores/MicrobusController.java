package com.example.alquiler.AlquilerCoches.Controladores;


import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import com.example.alquiler.AlquilerCoches.Modelos.Microbus;

import com.example.alquiler.AlquilerCoches.Servicios.servicioMicrobus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
public class MicrobusController {
    @Autowired
    servicioMicrobus serviceMicrobus;

    @GetMapping ({"/verMicrobuses"})
    public String microbuses(Model model, @ModelAttribute("message") String message){
        List<Microbus>microbuses= serviceMicrobus.getAllmicrobuss();
        model.addAttribute("microbuses",microbuses);
        model.addAttribute("message",message);
        return "GetAlquilerMicrobus";
    }
    @GetMapping("/microbus")
    public String crearMicrobus(Model model, @ModelAttribute("message") String message){
        Microbus microbus=new Microbus();
        model.addAttribute("microbus",microbus);
        model.addAttribute("message",message);
        return "agregarMicrobus";
    }
    @PostMapping("/guardarMicrobus")

    public String guardarMicrobus(Microbus microbus, RedirectAttributes redirectAttributes) {
        if (serviceMicrobus.crearMicrobus(microbus) == true) {
            redirectAttributes.addFlashAttribute("message", "saveOK");
            return "redirect:/verMicrobuses";
        }
        redirectAttributes.addFlashAttribute("message","saveError");
        return "redirect:/";
    }

    @GetMapping("/verMicrobus/{id}")
    public String getMicrobus(Model model, @PathVariable Integer id, @ModelAttribute("message") String message){
        Optional<Microbus> microbus= serviceMicrobus.GetMicrobusById(id);
        model.addAttribute("microbus",microbus);
        return "VerMicrobus";
    }

}
