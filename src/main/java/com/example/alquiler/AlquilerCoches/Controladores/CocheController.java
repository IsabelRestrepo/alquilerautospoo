package com.example.alquiler.AlquilerCoches.Controladores;

import com.example.alquiler.AlquilerCoches.Servicios.servicioCoche;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;
@Controller
public class CocheController {
   @Autowired
    servicioCoche serviceCoche;

    @GetMapping ({"/verCoches"})
    public String coches(Model model, @ModelAttribute("message") String message){
        List<Coche> coches= serviceCoche.getAllCoches();
        model.addAttribute("coches",coches);
        model.addAttribute("message",message);
        return "GetAlquilerCoche";
    }
    @GetMapping("/coche")
    public String crearCoche(Model model, @ModelAttribute("message") String message){
        Coche coche=new Coche();
        model.addAttribute("coche",coche);
        model.addAttribute("message",message);
        return "agregarCoche";
    }
    @PostMapping("/guardarCoche")

    public String guardarCoche(Coche coche, RedirectAttributes redirectAttributes) {
        if (serviceCoche.crearCoche(coche) == true) {
            redirectAttributes.addFlashAttribute("message", "saveOK");
         return "redirect:/verCoches";
        }
       redirectAttributes.addFlashAttribute("message","saveError");
        return "redirect:/";
    }
    @GetMapping("/verCoche/{id}")
    public String getCoche(Model model, @PathVariable Integer id, @ModelAttribute("message") String message){
        Optional<Coche> coche= serviceCoche.GetCarroById(id);
        model.addAttribute("coche",coche);
        model.addAttribute("message", message);
        return "VerCoche";
    }

    @GetMapping("/")
    public String home(){
        return "Home";
    }
}
