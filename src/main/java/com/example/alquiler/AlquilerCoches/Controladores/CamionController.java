package com.example.alquiler.AlquilerCoches.Controladores;


import com.example.alquiler.AlquilerCoches.Modelos.Camion;
import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import com.example.alquiler.AlquilerCoches.Servicios.servicioCamion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
public class CamionController {
    @Autowired
    servicioCamion serviceCamion;

    @GetMapping ({"/verCamiones"})
    public String camiones(Model model, @ModelAttribute("message") String message){
        List<Camion>camiones= serviceCamion.getAllCamiones();
        model.addAttribute("camiones",camiones);
        model.addAttribute("message",message);
        return "GetAlquilerCamion";
    }
    @GetMapping("/camion")
    public String crearCamion(Model model, @ModelAttribute("message") String message){
        Camion camion=new Camion();
        model.addAttribute("camion",camion);
        model.addAttribute("message",message);
        return "agregarCamion";
    }
    @PostMapping("/guardarCamion")

    public String guardarCamion(Camion camion, RedirectAttributes redirectAttributes) {
        if (serviceCamion.crearCamion(camion) == true) {
            redirectAttributes.addFlashAttribute("message", "saveOK");
            return "redirect:/verCamiones";
        }
        redirectAttributes.addFlashAttribute("message","saveError");
        return "redirect:/";
    }
    @GetMapping("/verCamion/{id}")
    public String getCamion(Model model, @PathVariable Integer id, @ModelAttribute("message") String message){
        Optional<Camion> camion= serviceCamion.GetCamionById(id);
        model.addAttribute("camion",camion);
        model.addAttribute("message", message);
        return "VerCamion";
    }
}
