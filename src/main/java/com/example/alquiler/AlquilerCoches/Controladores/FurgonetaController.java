package com.example.alquiler.AlquilerCoches.Controladores;

import com.example.alquiler.AlquilerCoches.Modelos.Camion;
import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import com.example.alquiler.AlquilerCoches.Modelos.Furgoneta;
import com.example.alquiler.AlquilerCoches.Servicios.servicioCamion;
import com.example.alquiler.AlquilerCoches.Servicios.servicioFurgoneta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
public class FurgonetaController {
    @Autowired
    servicioFurgoneta serviceFurgoneta;

    @GetMapping ({"/verFurgonetas"})
    public String coches(Model model, @ModelAttribute("message") String message){
        List<Furgoneta> furgonetas= serviceFurgoneta.getAllFurgonetas();
        model.addAttribute("furgonetas",furgonetas);
        model.addAttribute("message",message);
        return "GetAlquilerFurgoneta";
    }
    @GetMapping("/furgoneta")
    public String crearFurgoneta(Model model, @ModelAttribute("message") String message){
        Furgoneta furgoneta=new Furgoneta();
        model.addAttribute("furgoneta",furgoneta);
        model.addAttribute("message",message);
        return "agregarFurgoneta";
    }
    @PostMapping("/guardarFurgoneta")

    public String guardarFurgoneta(Furgoneta furgoneta, RedirectAttributes redirectAttributes) {
        if (serviceFurgoneta.crearFurgoneta(furgoneta) == true) {
            redirectAttributes.addFlashAttribute("message", "saveOK");
            return "redirect:/verFurgonetas";
        }
        redirectAttributes.addFlashAttribute("message","saveError");
        return "redirect:/";

    }
    @GetMapping("/verFurgoneta/{id}")
    public String getFurgoneta(Model model, @PathVariable Integer id, @ModelAttribute("message") String message){
        Optional<Furgoneta> furgoneta = serviceFurgoneta.GetFurgonetaById(id);
        model.addAttribute("furgoneta",furgoneta);
        model.addAttribute("message", message);
        return "VerFurgoneta";
    }




}
