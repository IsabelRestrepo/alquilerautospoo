package com.example.alquiler.AlquilerCoches.Repositorio;

import com.example.alquiler.AlquilerCoches.Modelos.Coche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CocheRepositorio extends JpaRepository<Coche,Integer> {
}
