package com.example.alquiler.AlquilerCoches.Repositorio;

import com.example.alquiler.AlquilerCoches.Modelos.Camion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CamionRepositorio extends JpaRepository<Camion, Integer> {
    }

