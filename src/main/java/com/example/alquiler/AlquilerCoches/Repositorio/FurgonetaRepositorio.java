package com.example.alquiler.AlquilerCoches.Repositorio;

import com.example.alquiler.AlquilerCoches.Modelos.Furgoneta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FurgonetaRepositorio extends JpaRepository<Furgoneta, Integer> {
    }
