package com.example.alquiler.AlquilerCoches.Repositorio;

import com.example.alquiler.AlquilerCoches.Modelos.Microbus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MicrobusRepositorio extends JpaRepository<Microbus, Integer> {
}
