package com.example.alquiler.AlquilerCoches.Modelos;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name="microbus")
@NoArgsConstructor
public class Microbus extends vehiculos{

    public Microbus(int id, String matricula, LocalDate fecha, String marca, String modelo, int totalDias, double total) {
        super(id, matricula, fecha, marca, modelo, totalDias, total);
    }

    @Override
    public double monto(){
        double total=(getTotalDias()*51.5)+2;
        return total;
    }

}
