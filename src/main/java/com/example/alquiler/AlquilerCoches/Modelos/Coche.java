package com.example.alquiler.AlquilerCoches.Modelos;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.http.converter.json.GsonBuilderUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;


@Setter
@Getter
@Entity
@Table(name="coche")
@NoArgsConstructor

public class Coche extends vehiculos {
    public Coche(int id, String matricula, LocalDate fecha, String marca, String modelo, int totalDias, double total, int id1) {
        super(id, matricula, fecha, marca, modelo, totalDias, total);
    }

    @Override
    public double monto(){
        double total=getTotalDias()*51.5;
        return total;
    }

}