package com.example.alquiler.AlquilerCoches.Modelos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;

import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name="camion")
@NoArgsConstructor
public class Camion extends vehiculos {
    public Camion(int id, String matricula, LocalDate fecha, String marca, String modelo, int totalDias, double total) {
        super(id, matricula, fecha, marca, modelo, totalDias, total);
    }
    private final int pma=15;

    @Override
    public double monto(){
        double total=((50*getTotalDias())+(20*pma)+40);
        return total;
    }

}
