package com.example.alquiler.AlquilerCoches.Modelos;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public abstract class vehiculos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String matricula;
    private LocalDate fecha= LocalDate.now();
    private String marca;
    private String modelo;
    private int totalDias;
    private double total=monto();

    public abstract double monto();

}