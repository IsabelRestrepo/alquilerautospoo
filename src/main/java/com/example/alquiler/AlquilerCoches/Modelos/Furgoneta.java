package com.example.alquiler.AlquilerCoches.Modelos;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name="furgoneta")
@NoArgsConstructor
public class Furgoneta extends vehiculos{
    public Furgoneta(int id, String matricula, LocalDate fecha, String marca, String modelo, int totalDias, double total) {
        super(id, matricula, fecha, marca, modelo, totalDias, total);
    }

    private final int pma=9;

    public double monto(){
        double total=((50*getTotalDias())+(20*pma));
        return total;
    }

}